﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Consid.ConferanceBooker.Models.Arne
{


    public class CreateCalendarRequest
    {
        public string Subject { get; set; }
        public Body Body { get; set; }
        public Start Start { get; set; }
        public End End { get; set; }
        public Attendee[] Attendees { get; set; }    

    }

    public class Start
    {
        public DateTime DateTime { get; set; }
        public string TimeZone { get; set; }
    }

    public class End
    {
        public DateTime DateTime { get; set; }
        public string TimeZone { get; set; }

    }





}
    


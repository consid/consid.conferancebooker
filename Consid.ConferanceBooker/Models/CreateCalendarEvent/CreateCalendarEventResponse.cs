﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Consid.ConferanceBooker.Models.CreateCalendarEvent
{    

    public class CreateCalendarEventResponse
    {
        public string odatacontext { get; set; }
        public string odataid { get; set; }
        public string odataetag { get; set; }
        public string Id { get; set; }
        public string ChangeKey { get; set; }
        public object[] Categories { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public DateTime LastModifiedDateTime { get; set; }
        public string Subject { get; set; }
        public string BodyPreview { get; set; }
        public Body Body { get; set; }
        public string Importance { get; set; }
        public bool HasAttachments { get; set; }
        public Start Start { get; set; }
        public End End { get; set; }
        public Location Location { get; set; }
        public string ShowAs { get; set; }
        public bool IsAllDay { get; set; }
        public bool IsCancelled { get; set; }
        public bool IsOrganizer { get; set; }
        public bool ResponseRequested { get; set; }
        public string Type { get; set; }
        public object SeriesMasterId { get; set; }
        public Attendee[] Attendees { get; set; }
        public object Recurrence { get; set; }
        public string OriginalEndTimeZone { get; set; }
        public string OriginalStartTimeZone { get; set; }
        public Organizer Organizer { get; set; }
        public object OnlineMeetingUrl { get; set; }
    }     
  

  

   

}

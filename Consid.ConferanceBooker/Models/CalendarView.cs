﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Consid.ConferanceBooker.Models
{
    
    public class CalendarView
    {
        public string odatacontext { get; set; }
        public CalendarEvent[] value { get; set; }
        public string odatanextLink { get; set; }
    }

    public class CalendarEvent
    {
        public string odataid { get; set; }
        public string odataetag { get; set; }
        public string Id { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public DateTime LastModifiedDateTime { get; set; }
        public string ChangeKey { get; set; }
        public object[] Categories { get; set; }
        public string OriginalStartTimeZone { get; set; }
        public string OriginalEndTimeZone { get; set; }
        public string iCalUId { get; set; }
        public int ReminderMinutesBeforeStart { get; set; }
        public bool IsReminderOn { get; set; }
        public bool HasAttachments { get; set; }
        public string Subject { get; set; }
        public string BodyPreview { get; set; }
        public string Importance { get; set; }
        public string Sensitivity { get; set; }
        public bool IsAllDay { get; set; }
        public bool IsCancelled { get; set; }
        public bool IsOrganizer { get; set; }
        public bool ResponseRequested { get; set; }
        public object SeriesMasterId { get; set; }
        public string ShowAs { get; set; }
        public string Type { get; set; }
        public string WebLink { get; set; }
        public string OnlineMeetingUrl { get; set; }
        public Responsestatus ResponseStatus { get; set; }
        public Body Body { get; set; }
        public Start Start { get; set; }
        public End End { get; set; }
        public Location Location { get; set; }
        public object Recurrence { get; set; }
        public Attendee[] Attendees { get; set; }
        public Organizer Organizer { get; set; }
    }

    public class Responsestatus
    {
        public string Response { get; set; }
        public DateTime Time { get; set; }
    }

    public class Body
    {
        public string ContentType { get; set; }
        public string Content { get; set; }
    }

    public class Start
    {
        public DateTime DateTime { get; set; }
        public string TimeZone { get; set; }

        public string PresentableDate { get
            {
                try
                {
                  
                      return  DateTime.ToString("yyyy-MM-dd HH':'mm':'ss");
                }
                catch
                {
                    return DateTime.ToString("yyyy-MM-dd HH':'mm':'ss");
                }
                
            }
        }
    }

    public class End
    {
        public DateTime DateTime { get; set; }
        public string TimeZone { get; set; }
        public string PresentableDate
        {
            get
            {
                try
                {

                    return DateTime.ToString("yyyy-MM-dd HH':'mm':'ss");
                }
                catch
                {
                    return DateTime.ToString("yyyy-MM-dd HH':'mm':'ss");
                }
            }
        }

    }

    public class Location
    {
        public string DisplayName { get; set; }
        public Address Address { get; set; }
        public Coordinates Coordinates { get; set; }
    }

    public class Address
    {
        public string Type { get; set; }
    }

    public class Coordinates
    {
    }

    public class Organizer
    {
        public Emailaddress EmailAddress { get; set; }
    }

    public class Emailaddress
    {
        public string Name { get; set; }
        public string Address { get; set; }
    }

    public class Attendee
    {
        public string Type { get; set; }
        public Status Status { get; set; }
        public Emailaddress1 EmailAddress { get; set; }
    }

    public class Status
    {
        public string Response { get; set; }
        public DateTime Time { get; set; }
    }

    public class Emailaddress1
    {
        public string Name { get; set; }
        public string Address { get; set; }
    }


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Consid.ConferanceBooker.Infrastructure;
using Newtonsoft.Json;
using Consid.ConferanceBooker.Models;
using Consid.ConferanceBooker.Models.CreateCalendarEvent;
using Consid.ConferanceBooker.Models.Arne;
using System.Globalization;

namespace Consid.ConferanceBooker.Controllers
{
    public class ConferanceController : Controller
    {

        

        [HttpGet]
        [Route("Index")]
        public async Task<IActionResult> Index()
        {
            ViewBag.KonferansRum = DataHolder.Instance.ConferanceName;

            return View("ListTodaysBookings");
        }



        [HttpGet]
        [Route("GetEvents")]
        public async Task<JsonResult> GetTodaysEvents()
        {
            try
            {
                DateTime startDate = DateTime.Now.AddHours(-2);
                DateTime endDate = DateTime.Now.AddDays(30).Date;

                var calenderView = await RequestHelper.GetCalendarViewAsync(startDate, endDate);

                if (!string.IsNullOrEmpty(calenderView))
                {
                    var calendarViewResult = JsonConvert.DeserializeObject<CalendarView>(calenderView);

                    

                    if (calendarViewResult != null && calendarViewResult.value != null && calendarViewResult.value.Any())
                    {


                        foreach (var row in calendarViewResult.value)
                        {
                           string tz = "W. Europe Standard Time";

                            TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(tz);
                            row.Start.DateTime = TimeZoneInfo.ConvertTimeFromUtc(row.Start.DateTime, cstZone);
                            row.End.DateTime = TimeZoneInfo.ConvertTimeFromUtc(row.End.DateTime, cstZone);
                        }

                        var calendarViewSortedAndAccepted = calendarViewResult
                            .value
                            .Where(X => X.ResponseStatus.Response.ToUpper() == "Accepted".ToUpper() || X.ResponseStatus.Response.ToUpper() == "Organizer".ToUpper())
                            .OrderBy(X => X.Start.DateTime)
                            .Take(10)
                            .ToList();

                        DataHolder.Instance.CurrentCalendarEvents = calendarViewSortedAndAccepted;

                        return Json(JsonConvert.SerializeObject(calendarViewSortedAndAccepted));
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return Json("");
        }

        [HttpPut]
        [Route("CreateEvent")]
        public async Task<CreateCalendarEventResponse> CreateEvent()
        {         
            
            try
            {
                string tz = TimeZone.CurrentTimeZone.StandardName;
                var currentCalendarViewFirst = DataHolder.Instance.CurrentCalendarEvents.FirstOrDefault();
                DateTime? nextEvent = currentCalendarViewFirst?.Start.DateTime;
                var request = new CreateCalendarRequest();
                request.Start = new Models.Arne.Start();
                request.Start.DateTime = DateTime.Now;
                request.Start.TimeZone = tz;
                request.Subject = "Booked by PI";
                request.Body = new Body()
                {
                    Content = "Booked by pi",
                    ContentType = "text"
                };
                request.Attendees = new List<Attendee>()
                {
                    new Attendee()
                    {
                        EmailAddress = new Emailaddress1()
                        {
                            Address = DataHolder.Instance.EmailAddress,
                            Name =  DataHolder.Instance.ConferanceName
                        }
                    }
                }.ToArray();

                if (nextEvent == null)
                {                 
                    request.End = new Models.Arne.End()
                    {
                        DateTime = DateTime.Now.AddHours(1),
                        TimeZone = tz
                    };                  
                }
                else
                {
                    DateTime endDate = DateTime.Now.AddHours(1);
                    if(endDate > nextEvent.Value.AddMinutes(-1))
                    {
                        endDate = nextEvent.Value.AddMinutes(-1);
                    }

                    request.End = new Models.Arne.End()
                    {
                        DateTime = endDate,
                        TimeZone = tz
                    };                   
                }

               
                var createCalendarEventResponse = await RequestHelper.CreateEvent(request);
                return createCalendarEventResponse;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return new CreateCalendarEventResponse();
        }

      public static DateTime UnixTimeStampToDateTime( double unixTimeStamp )
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970,1,1,0,0,0,0,System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds( unixTimeStamp ).ToUniversalTime();
            return dtDateTime;
        }

        public static DateTime JavaTimeStampToDateTime( double javaTimeStamp )
        {
            // Java timestamp is milliseconds past epoch
            System.DateTime dtDateTime = new DateTime(1970,1,1,0,0,0,0,System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddMilliseconds( javaTimeStamp ).ToUniversalTime();
            return dtDateTime;
        }


        [HttpPut]
        [Route("Create")]
        public async Task<CreateCalendarEventResponse> CreateEvent(double start, double end)
        {

            try
            {
                DateTime startTime = JavaTimeStampToDateTime(start);
                DateTime endTime = JavaTimeStampToDateTime(end);
                string tz = TimeZone.CurrentTimeZone.StandardName;


                if (startTime != DateTime.MinValue && endTime != DateTime.MinValue)
                {
                    var currentCalendarViewFirst = DataHolder.Instance.CurrentCalendarEvents.FirstOrDefault();
                    DateTime? nextEvent = currentCalendarViewFirst?.Start.DateTime;

                  
                        var request = new CreateCalendarRequest();
                        request.Start = new Models.Arne.Start();
                        request.Start.DateTime = startTime;
                        request.Start.TimeZone = tz;
                        request.Subject = "Booked by PI";
                        request.Body = new Body()
                        {
                            Content = "Booked by pi",
                            ContentType = "text"
                        };
                        request.Attendees = new List<Attendee>()
                        {
                            new Attendee()
                            {
                                EmailAddress = new Emailaddress1()
                                {
                                    Address = DataHolder.Instance.EmailAddress,
                                    Name = DataHolder.Instance.ConferanceName
                                }
                            }
                        }.ToArray();

                        request.End = new Models.Arne.End()
                        {
                            DateTime = endTime,
                            TimeZone = tz
                        };


                        var createCalendarEventResponse = await RequestHelper.CreateEvent(request);
                        return createCalendarEventResponse;
                    
                }

                throw new Exception("Invalid dates!");
               
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return new CreateCalendarEventResponse();
        }


    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Identity.Client;
using Microsoft.Net.Http.Headers;
using Consid.ConferanceBooker.Infrastructure;
using Newtonsoft.Json;
using Consid.ConferanceBooker.Models;
using System.Threading;

namespace Consid.ConferanceBooker.Controllers
{
    [Route("api/[controller]")]
    public class AuthenticationController : Controller
    {
        [HttpGet]
        [Route("Auth")]
        public async Task<ActionResult> Index()
        {
            return Redirect(RequestHelper.GetRequestUrl());
        }

        [HttpGet]
        [Route("Callback")]
        public async Task<ActionResult> Callback(string code, string state,string error ,string error_description)
        {
            var tokenAsString = await RequestHelper.GetToken(code);

            if(!string.IsNullOrEmpty(tokenAsString))
            {
                var holder = DataHolder.Instance;
                holder.TokenModel = JsonConvert.DeserializeObject<Token>(tokenAsString);

                new Thread(RequestHelper.MakeSureAccessTokenIsRefreshed).Start();
                return Redirect(DataHolder.Instance.success_url);
            }

           

            return Content(error + Environment.NewLine + error_description);
        }

     
    }
}

$(document).ready(function() {
 

  function add0ToHour(timenow){
    if (timenow.getHours() < 10)
    {
      return hours = '0' + timenow.getHours();
    }
  }
  
  function generateMomentBtnCont(timenow) {
    
    if (timenow.getMinutes() <= 30) {
      $("#momentBtn1").html(
        timenow.getHours() + ":00 - " + timenow.getHours() + ":30"
      );
      $("#momentBtn2").html(
        timenow.getHours() + ":30 - " + (timenow.getHours() + 1) + ":00"
      );
      $("#momentBtn3").html(
        timenow.getHours() + 1 + ":00 - " + (timenow.getHours() + 1) + ":30"
      );
      $("#momentBtn4").html(
        timenow.getHours() + 1 + ":30 - " + (timenow.getHours() + 2) + ":00"
      );
    } else {
      $("#momentBtn1").html(
        timenow.getHours() + ":30 - " + (timenow.getHours() + 1) + ":00"
      );
      $("#momentBtn2").html(
        timenow.getHours() + 1 + ":00 - " + (timenow.getHours() + 1) + ":30"
      );
      $("#momentBtn3").html(
        timenow.getHours() + 1 + ":30 - " + (timenow.getHours() + 2) + ":00"
      );
      $("#momentBtn4").html(
        timenow.getHours() + 2 + ":00 - " + (timenow.getHours() + 2) + ":30"
      );
    }
  }

  
  function ledigOrUpptagen(json, timenow) {
      var arr = [];
        for (i = 0; i < json.length; i++) {
          
           var startData = new Date(json[i].Start.PresentableDate);
          var endData = new Date(json[i].End.PresentableDate) ;
         
         
          
          if (timenow.getDate() == startData.getDate()) {
            //check if  we have same day
            var objBooked = { 
              startDate: startData,
              endDate: endData
            };
            arr.push(objBooked);
           
          }
        }

        for(j=0; j < arr.length; j++){
          
         
          if (
            arr[j].startDate.getTime() <= timenow.getTime() &&
            arr[j].endDate.getTime() > timenow.getTime()
          ) {
            $("#roomState").html("Upptagen");
            $("#roomState").css("color", "#d31f31");
            $("#momentBtn1").addClass("timebtnBooked");
            $("#momentBtn1").removeClass("timebtn1");
          }
            var endCell = new Date(timenow.valueOf());
  
            if (endCell.getMinutes() <= 30) {
              endCell.setMinutes(0);
            } else {
              endCell.setMinutes(30);
             
            }
            endCell.setSeconds(00);
            endCell.setMilliseconds(00);
           endCell.setHours(endCell.getHours() + 1);
           var startCell = new Date(endCell.valueOf());
           startCell.setMinutes(endCell.getMinutes() - 30);
           
            if (startCell >= arr[j].startDate && endCell <= arr[j].endDate) {
              $("#momentBtn2").addClass("timebtnBooked");
              $("#momentBtn2").removeClass("timebtn2");
             
            }
            startCell.setMinutes(startCell.getMinutes() + 30);
            endCell.setMinutes(endCell.getMinutes() + 30);
            if (startCell >= arr[j].startDate && endCell <= arr[j].endDate) {
                $("#momentBtn3").addClass("timebtnBooked");
                $("#momentBtn3").removeClass("timebtn3");
              
              }
              startCell.setMinutes(startCell.getMinutes() + 30);
              endCell.setMinutes(endCell.getMinutes() + 30);
            if (startCell >= arr[j].startDate && endCell <= arr[j].endDate) {
                  $("#momentBtn4").addClass("timebtnBooked");
                  $("#momentBtn4").removeClass("timebtn4");
                
                }
                
            
          
          // f end
        }
      }

 

  
  function clockTimeNow() {
    var timenow = new Date();
    
    var hours = timenow.getHours();
    var minutes = timenow.getMinutes();
    
   
    if(timenow.getMinutes() <10)
    { minutes = '0' + timenow.getMinutes(); }
    $("#time").html(hours + ":" + minutes);
    return timenow;
  }
  function todayWithoutTime(){ 
    
          
    var date = new Date();
    var day = date.getDate();       // yields date
    var month = date.getMonth() + 1;    // yields month (add one as '.getMonth()' is zero indexed)
    var year = date.getFullYear();  // yields year		
    var time =  month + "/" + day + "/" + year;
    return time;   
     
    }


    function bookTid (celltid){
      if(celltid.length < 13){
        var startTime = celltid.substring(0, 4);
        var endTime = celltid.slice(7, 13);
      }else{
        var startTime = celltid.substring(0, 5);
        var endTime = celltid.slice(8, 13);
      }
      
      
  
      var StartDate = todayWithoutTime() + " " + startTime + ":00" ;
	  var startDateSince1970 = new Date(StartDate).getTime();
	  
	  
      var EndDate = todayWithoutTime() + " " + endTime + ":00" ;
	  var endDateSince1970 = new Date(EndDate).getTime();
	  var urlVariableWithParams = "https://considconferancebooker.azurewebsites.ne/Create?start="+ startDateSince1970 + "&end=" + endDateSince1970;
      
      $.ajax({
        type: 'PUT',
        
        url: urlVariableWithParams,
        error: function(XMLHttpRequest, textStatus, errorThrown) {
          alert("some error");
       },
        success:function(response) {
  
          var responseJson = jQuery.parseJSON(response);
  
         
          $(".medelande").show().delay( 4000 ).hide( 0 );
         
      }
    });
    }

  function showMesseage(){
    $(".medelande").html('<span class="alert alert-info">Din tid är bookad</span>');
  }

  function clickBokaTid() {
    $("#bokaTid").click(function() {
      // take all with class timebtnChecked and send
      if ($("#momentBtn1").hasClass("timebtnChecked")) {
        $("#momentBtn1").removeClass("timebtnChecked");
        bookTid($("#momentBtn1").text());
       
      }
     
      if ($("#momentBtn2").hasClass("timebtnChecked")) {
        bookTid($("#momentBtn2").text());
        $("#momentBtn2").removeClass("timebtnChecked");
      }
      
      if ($("#momentBtn3").hasClass("timebtnChecked")) {
        bookTid($("#momentBtn3").text());
        $("#momentBtn3").removeClass("timebtnChecked");
      }
      
      if ($("#momentBtn4").hasClass("timebtnChecked")) {
        bookTid($("#momentBtn4").text());
        $("#momentBtn4").removeClass("timebtnChecked");
      }
     
      
    });
  }

  $( "#bokaTid" ).click(function() {
    var howManyButtons=0;
    if ($("#momentBtn1").hasClass("timebtnChecked")) {
      bookTid($("#momentBtn1").text());
      $("#momentBtn1").removeClass("timebtnChecked");
      howManyButtons += howManyButtons;
    }
   
    if ($("#momentBtn2").hasClass("timebtnChecked")) {
      bookTid($("#momentBtn2").text());
      $("#momentBtn2").removeClass("timebtnChecked");
      howManyButtons = 1;
    }
    
    if ($("#momentBtn3").hasClass("timebtnChecked")) {
      bookTid($("#momentBtn3").text());
      $("#momentBtn3").removeClass("timebtnChecked");
      howManyButtons = 1;
    }
    
    if ($("#momentBtn4").hasClass("timebtnChecked")) {
      bookTid($("#momentBtn4").text());
      $("#momentBtn4").removeClass("timebtnChecked");
      howManyButtons = 1;
    }
    if(howManyButtons == 0){
      $(".medelande1").show().delay( 4000 ).hide( 0 );
    }
  });
   
  
  $( "#momentBtn1" ).click(function() {
    $(".timebtn1" ).toggleClass( "timebtnChecked" );
    
  });
  $( "#momentBtn2" ).click(function() {
    $(".timebtn2" ).toggleClass( "timebtnChecked" );
    
  });
  $( "#momentBtn3" ).click(function() {
    $(".timebtn3" ).toggleClass( "timebtnChecked" );
    
  });
  $( "#momentBtn4" ).click(function() {
    $(".timebtn4" ).toggleClass( "timebtnChecked" );
    
  });
 
    $.ajax({
      type: "GET",
      url: "https://considconferancebooker.azurewebsites.net/getevents",
      data: { get_param: "value" },
      dataType: "json",
      success: function(data) {
        var json = jQuery.parseJSON(data);
        var timenow = clockTimeNow();
       
        ledigOrUpptagen(json, timenow);
        generateMomentBtnCont(timenow);
       
      }
    });
  
 
    function init(){
    
      $.ajax({
        type: "GET",
        url: "https://considconferancebooker.azurewebsites.net/getevents",
        data: { get_param: "value" },
        dataType: "json",
        success: function(data) {
          var json = jQuery.parseJSON(data);
          var timenow = clockTimeNow();
         
          ledigOrUpptagen(json, timenow);
          generateMomentBtnCont(timenow);
         
        }
      });
  }
  $(".medelande").hide();
  $(".medelande1").hide();
  setInterval(init, 60000);
});

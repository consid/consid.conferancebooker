﻿function ConferanceBooker(urlToEvents,urlToCreateEvents) {
	this.construct = function (builder) {
		builder.step1(urlToEvents, urlToCreateEvents, builder.step2);				
	}
}


function ConferanceViewBuilder() {
	

	this.step1 = function (urlToEvents, urlToCreateEvents,cb) {	

		    var events = $.get(urlToEvents,			
			function (result) {
				if (result != null && result.length > 0) {
					var obj = jQuery.parseJSON(result);
					this.model = obj;
				}
			});

		 events.always(function () {

		 if (typeof cb === "function") {
			 cb(this.model);
		 }});

			
		}

	

	this.step2 = function (model) {
		

		$.each(model, function (index, event) {					
			var row = $('<tr>');
			row.append($('<td>').append($('<span>').text(event.Start.PresentableDate)));
			row.append($('<td>').append($('<span>').text(event.End.PresentableDate)));
			row.append($('<td>').append($('<span>').text(event.Organizer.EmailAddress.Name
				+ "(" + event.Organizer.EmailAddress.Address + ")")));		
			$("#eventTable tbody").append(row);
		});	
	};
	
}

function displayNewBookingForm() {
	$("#bookingsListContainer").fadeOut("slow");
	$("#createEventFormContainer").fadeIn("slow");
}


function displayEventList() {
	$("#createEventFormContainer").fadeOut("slow");
	$("#bookingsListContainer").fadeIn("slow");

}



function ConferanceCreator(url) {

	$.ajax({
		url: url,
		type: 'PUT',
		success: function (response) {
			location.reload();
		}
	});

}
function Initiate(getEventUrl, createEventUrl) {
	var booker = new ConferanceBooker(getEventUrl, createEventUrl);
	var viewBuilder = new ConferanceViewBuilder();	
	var viewModel = booker.construct(viewBuilder);	

	//$(function () {
	//	$('#dateTimePicker').datetimepicker({
	//		inline: true,
	//		sideBySide: true,
	//		daysOfWeekDisabled: [0, 6]
	//	});
	//});

	//$("#btnNewBooking").bind('click', );
	//$("#btnCancel").on('click', displayEventList());
	//$("#btnNewBooking").on('click', ConferanceCreator());
}
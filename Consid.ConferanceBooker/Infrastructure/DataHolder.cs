﻿using Consid.ConferanceBooker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Consid.ConferanceBooker.Infrastructure
{
    public class DataHolder
    {
        public string EmailAddress { get; set; }
        public string ConferanceName { get; set; }
        public string clientId;
        public string redirectUri;
        public string password;
        public string success_url { get; set; }


        public DataHolder()
        {

#if DEBUG
            EmailAddress = "malmoglimmingehus@consid.se";
            ConferanceName = "Glimmingehus";
            clientId = "e065c5d8-4e92-4247-acfd-449cbee5ae43";
            redirectUri = "https://localhost:44324/api/Authentication/Callback";
            password = "iaxkREAE51=}znfNRU264;/";
            success_url = "https://localhost:44324/index";

#endif

#if GLIMMINGEHUS
            EmailAddress = "malmoglimmingehus@consid.se";
            ConferanceName = "Glimmingehus";
            clientId = "e065c5d8-4e92-4247-acfd-449cbee5ae43";
            redirectUri = "https://considconferancebooker.azurewebsites.net/api/Authentication/Callback";
            password = "iaxkREAE51=}znfNRU264;/";
            success_url = "https://considconferancebooker.azurewebsites.net/index";
#endif

#if MALMOHUS
            EmailAddress = "malmoglimmingehus@consid.se";
            ConferanceName = "Glimmingehus";
            clientId = "5c031ca3-4516-459d-9d3a-cbc6dc70f493";
            redirectUri = "https://considconferancebooker-malmohus.azurewebsites.net/api/Authentication/Callback";
            password = "xtclztK24(|?rYCMCLQ387<";
            success_url = "https://considconferancebooker-malmohus.azurewebsites.net/index";
#endif
        }




        private static List<CalendarEvent> _currentCalendarEvents = new List<CalendarEvent>();
        public List<CalendarEvent> CurrentCalendarEvents
        {
            get
            {
                return _currentCalendarEvents;
            }
            set
            {
                _currentCalendarEvents = value;
            }
        }

        private static Token _tokenModel = new Token();
        public Token TokenModel
        {
            get
            {
                return _tokenModel;
            }
            set
            {
                _tokenModel = value;
            }
        }

        private static DataHolder _instance = new DataHolder();

        public static DataHolder Instance
        {
            get
            {
                return _instance;
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Consid.ConferanceBooker.Infrastructure
{
    public static class ObjectExtensions
    {
        public static DateTime GetValidDateTime(this object o, DateTime defaultValue)
        {
            if(o == null || !DateTime.TryParse(o.ToString(), out DateTime result))
            {
                return defaultValue;
            }

            if(result == DateTime.MinValue || result == DateTime.MaxValue)
            {
                return defaultValue;
            }

            return result;

        }


        public static DateTime FromUnixTime(this long unixTime)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddSeconds(unixTime);
        }

    }
}

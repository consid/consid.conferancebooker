﻿using Consid.ConferanceBooker.Models;
using Consid.ConferanceBooker.Models.Arne;
using Consid.ConferanceBooker.Models.CreateCalendarEvent;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Consid.ConferanceBooker.Infrastructure
{
    public class RequestHelper
    {
        string[] _scopes = new string[] { "calendars.read" };
        public static string tenant = "common";
        public static string scope = "calendars.read offline_access calendars.readwrite";   

        public static DateTime start_datetime = DateTime.Now;
        public static DateTime end_datetime = DateTime.Now.AddMonths(1);

        public static string clientId = DataHolder.Instance.clientId;
        public static string redirectUri = DataHolder.Instance.redirectUri;
        public static string password =  DataHolder.Instance.password;




        public static string GetRequestUrl()
        {
            var query = $"https://login.microsoftonline.com/{tenant}/oauth2/v2.0/authorize?" +
                $"client_id={clientId}&" +
                $"response_type=code&" +
                $"redirect_uri={redirectUri}&" +
                $"response_mode=query&" +
                $"scope={scope}&" +
                $"state=12345";

            return query;
        }

        public static async Task<string> GetHttpContentWithToken(string url, string token)
        {
            var httpClient = new System.Net.Http.HttpClient();
            System.Net.Http.HttpResponseMessage response;
            try
            {
                var request = new System.Net.Http.HttpRequestMessage(System.Net.Http.HttpMethod.Get, url);
                //Add the token in Authorization header
                request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
                response = await httpClient.SendAsync(request);
                var content = await response.Content.ReadAsStringAsync();
                return content;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }


        internal static async Task<string> GetCalendarViewAsync(DateTime from, DateTime to)
        {
            from = from.GetValidDateTime(DateTime.Now);
            to = to.GetValidDateTime(DateTime.Now);

            string url = $"https://outlook.office.com/api/v2.0/me/calendarview?startDateTime={from}&endDateTime={to}";

            var httpClient = new System.Net.Http.HttpClient();
            System.Net.Http.HttpResponseMessage response;
            try
            {
                var request = new System.Net.Http.HttpRequestMessage(System.Net.Http.HttpMethod.Get, url);
                //Add the token in Authorization header
                request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", DataHolder.Instance.TokenModel.access_token);
                response = await httpClient.SendAsync(request);
                var content = await response.Content.ReadAsStringAsync();
                return content;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }

        }


        internal static async Task<CreateCalendarEventResponse> CreateEvent(CreateCalendarRequest request)
        {          
            string url = $"https://outlook.office.com/api/v2.0/me/events";
            var httpClient = new System.Net.Http.HttpClient();
            try
            {

                var stringContent = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");
                httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", DataHolder.Instance.TokenModel.access_token);
                var response = await httpClient.PostAsync(url, stringContent);

                
                if(response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<CreateCalendarEventResponse>(content);
                }

                return new CreateCalendarEventResponse();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return new CreateCalendarEventResponse();
            }

        }


        public static async Task<string> GetToken(string code)
        {
            var httpClient = new HttpClient();
            HttpResponseMessage response;
            try
            {

                var url = "https://login.microsoftonline.com/common/oauth2/v2.0/token";
                var parameters = new Dictionary<string, string> {
                    { "grant_type", "authorization_code" },
                    { "client_id", clientId },
                    { "scope", scope },
                    { "code", code },
                    { "redirect_uri", redirectUri },
                    { "client_secret", password },
                    
                };
                var encodedContent = new FormUrlEncodedContent(parameters);

                response = await httpClient.PostAsync(url, encodedContent).ConfigureAwait(false);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    return content;
                }

                return "No token found";
             
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public static void MakeSureAccessTokenIsRefreshed()
        {
            while (true)
            {
                try
                {
                    var dataHolderInstance = DataHolder.Instance;

                    if (!string.IsNullOrEmpty(dataHolderInstance.TokenModel.refresh_token))
                    {

                        var tokenAsString = RequestHelper.RefreshToken().Result;

                        if (!string.IsNullOrEmpty(tokenAsString))
                        {
                            var holder = DataHolder.Instance;
                            holder.TokenModel = JsonConvert.DeserializeObject<Token>(tokenAsString);
                        }
                    }
                }
                catch
                {

                }


                Thread.Sleep(500000);
            }
        }


        public static async Task<string> RefreshToken()
        {
            var httpClient = new HttpClient();
            HttpResponseMessage response;
            try
            {

                var url = "https://login.microsoftonline.com/common/oauth2/v2.0/token";
                var parameters = new Dictionary<string, string> {
                    { "grant_type", "refresh_token" },
                    { "client_id", clientId },
                    { "scope", scope },
                    { "refresh_token", DataHolder.Instance.TokenModel.refresh_token },
                    { "redirect_uri", redirectUri },
                    { "client_secret", password },

                };
                var encodedContent = new FormUrlEncodedContent(parameters);

                response = await httpClient.PostAsync(url, encodedContent).ConfigureAwait(false);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    return content;
                }

                return "No token found";

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }



    }
}
